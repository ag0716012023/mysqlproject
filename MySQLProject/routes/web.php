<?php

use App\Http\Controllers\TipoCuentaController;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/tables', function () {
    return view('tables.tables');
});


Route::get('/typeOfAccounts', [TipoCuentaController::class, "index"]);

Route::post('/create-typeAccount', [TipoCuentaController::class, "create"])->name('create.typeAccount');
Route::post('/update-typeAccount/{id}', [TipoCuentaController::class, "update"])->name('update.typeAccount');

Route::get('/delete-typeAccount/{id}', [TipoCuentaController::class, "destroy"])->name('delete.typeAccount');
